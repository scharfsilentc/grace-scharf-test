import React, { Component } from 'react';
import styled from 'styled-components'
import attributes from './attributes.js'
import models from './models.js'
import SavedFilters from './SavedFilters'
const FontAwesome = require('react-fontawesome')


// const days = [7, 14, 30, 90, 182, 365]
// const km = [100, 300, 900, 1200, 3000]


//styled components
const ComponentWrapper = styled.div`
  margin: 2%;
  font-family: 'Open Sans', sans-serif;
`

const FilterContainer = styled.div`
   display: grid;
   grid-template-rows: minmax(3rem,auto) minmax(4rem,auto);
   grid-template-columns: 1fr 1fr 1fr;
   grid-column-gap: 3rem;
   grid-row-gap: 1rem;
`
const HeadingWrapper = styled.div`
  grid-column: 1 / 4;
`
const HeaderDiv = styled.div`
  display: flex;
`
// would an h be better for seo? i chose p because of its inate stylings
const Heading = styled.p`
  margin: 1rem;
  color: grey;
`

const FilterWrapper = styled.div`
  margin: 10px 0;
    ${'' /* display: grid;
    grid-template-rows: 1fr auto 1fr 60px; */}
`

const LabelContainer = styled.div`
  width: 25rem;
  box-shadow: 0px 0px 4px 0px rgba(145, 145, 145, 0.2);
  border: 2.3px solid rgb(205,205,205);
  padding: .8rem .7rem .4rem .7rem;
  display: grid;
  grid-template-columns: 7fr .5fr 0.3fr;
`
const ListRenderer = styled.div`
    margin: 0 0 0 0.4rem;
    padding: 0;
    width: 25.5rem;
    box-shadow: -1px 2px 5px 0px rgba(141,141,141,0.54);
`
const List = styled.ul`
  list-style-type: none;
  padding: 0;
`
const ListItem = styled.li`
    padding: 4%;
    &:hover {
      background-color: rgba(148, 174, 250, 0.1);
    } 
`
const Form = styled.form`
      padding: 15px;
`
const Input = styled.input`
    padding: 7px;
    width: 20%;
    font-size: 1rem;
    border: none;
    ${'' /* border: 1px solid #80808059; */}
`
const ExpandButton = styled(FontAwesome) `
      margin-left: 3rem;
      color:lightgrey
`

const InputWrapper = styled.div`
  padding: 5px 0 5px 9px;
    border: 2px solid lightgrey;
    border-radius: 4px;
`
const SearchIcon = styled(FontAwesome) `
  color: lightgrey;
  padding-right: 10px;

`
const SaveButton = styled.button`
    border-radius: 4px;
    font-family: Open Sans,sans-serif;
    padding: 10px 18px;
    font-size: 1rem;
    margin-left: 5rem;
    &:hover{
      background-color: rgba(148, 174, 250, 0.1);
    }
`
const DatePicker = styled.input`
    font-size: 1.2rem;
    border: none;
    font-family: "PT Sans",sans-serif;
    width: 11rem;
`

class Filter extends Component {
  constructor() {
    super();
    this.state = {
      attributeSelected: "",
      operatorSelected: "",
      inputSelected: "",
      inputNumber: 0,
      inputType: "",
      hideOperator: true,
      hideInput: true,
      hideAttributeList: true,
      hideOperatorList: true,
      hideInputList: false,
      filterList: []


    }
  }

  // checks to make sure that attributes file has been loaded, 
  //then loops over attribute object keys and renders them each as a list item
  // 'title' is used to target the element
  renderAttributesList = (key) => {
    if (attributes) {
      return (
        <ListItem title={key} onClick={this.selectAttribute}>{key}</ListItem>
      )
    }
  }
  renderOperatorList = (key) => {
    return (
      <ListItem title={key} onClick={this.selectOperator}>{key}</ListItem>
    )
  }

  renderInputList = (v, type) => {
    return (
      <ListItem title={v} onClick={this.selectInput}>{v} {type}</ListItem>
    )
  }


  selectAttribute = (e) => {
    let title = e.currentTarget.title;
    this.setState({
      attributeSelected: title,
    }, function () {
      this.hideAttributeList(true)
      this.hideOperatorSection(false);
      this.hideOperatorList(false);
    })
  }

  selectOperator = (e) => {
    let title = e.currentTarget.title;
    this.setState({
      operatorSelected: title,
      inputType: attributes[this.state.attributeSelected][title],
    },
      function () {
        if (attributes[this.state.attributeSelected][title] === null) {
          this.saveFilter()
        } else {
          this.hideOperatorList(true);
          this.hideInputSection(false)
        }
      }
    )
  }

  selectInput = (e) => {
    let title = e.currentTarget.title;
    this.setState({
      inputSelected: title
    }, this.saveFilter)
  }

  hideAttributeList = (bool) => {
    this.setState(st => ({
      hideAttributeList: bool
    }))
  }

  hideOperatorSection = (bool) => {
    this.setState(st => ({
      hideOperator: bool
    }))
  }
  hideOperatorList = (bool) => {
    this.setState(st => ({
      hideOperatorList: bool
    }))
  }

  hideInputSection = (bool) => {
    this.setState(st => ({
      hideInput: bool
    }))
  }

  hideInputList = (bool) => {
    this.setState(st => ({
      hideInputList: bool
    }))
  }

  toggleAttributeList = (e) => {
    if (this.state.hideAttributeList) {
      this.setState({ hideAttributeList: false })
    } else
      this.setState({ hideAttributeList: true })
  }

  toggleOperatorList = (e) => {
    if (this.state.hideOperatorList) {
      this.setState({ hideOperatorList: false })
    } else
      this.setState({ hideOperatorList: true })
  }


  clearPreviousFilterState = () => {
    console.log('Here is the array that would be sent to the server:', this.state.filterList)
    this.setState(st => (
      {
        attributeSelected: "",
        operatorSelected: "",
        inputSelected: "",
        inputNumber: 0,
        inputType: "",
        hideInput: true,
        hideOperator: true,
      }
    ))
  }

  // this function stores all the seperate elements of the filter into an object
  // and adds a filterid so that it's possible to delete the filter later on
  // the object is then concatonated into an array of all the filters
  // so that it's easy to send them to the backend.
  saveFilter = (e) => {
    const filterId = Math.floor(Math.random() * 60000)
    const filter = {
      attribute: this.state.attributeSelected,
      operator: this.state.operatorSelected,
      filterId
    }
    if (this.state.inputNumber) {
      filter.input = (this.state.inputNumber + " " + this.state.inputType)
    } else if (this.state.inputSelected) {
      filter.input = this.state.inputSelected
    }
    console.log('new filter: ', filter)
    this.setState({
      filterList: this.state.filterList.concat(filter)
    },
      this.clearPreviousFilterState
    )
  }

  deleteFilter = (e) => {
    const id = e.target.title;
    const newState = this.state.filterList.slice(0); // doing this bc i only want to change the state via setstate
    for (let i = 0; i < newState.length; i++) {
      if (newState[i]['filterId'] == id) {
        newState.splice(i, 1)
      }
    }
    this.setState(st => ({ filterList: newState }))
    console.log('Filter deleted')
  }



  render() {
    return (
      <ComponentWrapper>
        <FilterContainer>
          <HeadingWrapper>
            <HeaderDiv >
              <img style={{ height: 50 }} alt="3 in a circle" src="https://png.icons8.com/circled-3/ios7/50/cccccc" />
              <Heading>ADD ATTRIBUTES</Heading>
            </HeaderDiv>
            <SavedFilters filterList={this.state.filterList} deleteFilter={this.deleteFilter} />
          </HeadingWrapper>

          {/* col 1: attribute */}
          <FilterWrapper>
            <LabelContainer>
              <span>{this.state.attributeSelected ? this.state.attributeSelected : "Select an attribute or an existing segment"}</span>
              <ExpandButton name="angle-down" size="2x" onClick={this.toggleAttributeList} />
            </LabelContainer>

            <ListRenderer hidden={this.state.hideAttributeList}>
              <List>
                {Object.keys(attributes).map(key => this.renderAttributesList(key))}
              </List>
            </ListRenderer>

          </FilterWrapper>

          {/* col 2: operator */}
          <FilterWrapper hidden={this.state.hideOperator}>
            <LabelContainer>
              <span>{this.state.operatorSelected ? this.state.operatorSelected : "Select a condition"}</span>
              <ExpandButton name="angle-down" size="2x" onClick={this.toggleOperatorList} />
            </LabelContainer>

            <ListRenderer hidden={this.state.hideOperatorList}>
              <List>
                {this.state.attributeSelected ? Object.keys(attributes[this.state.attributeSelected]).map(key => this.renderOperatorList(key)) : null}
              </List>
            </ListRenderer>

          </FilterWrapper>

          {/* col 3: input */}

          <FilterWrapper hidden={this.state.hideInput}>
            <LabelContainer>
              <span>{this.state.inputSelected ? this.state.inputSelected : "Add a value"} </span>
            </LabelContainer>

            <ListRenderer hidden={this.state.hideInputList}>
              {this.state.inputType === 'days' || this.state.inputType === 'km'
                ?
                  (<List>
                    <Form onSubmit={this.saveFilter}>
                      <InputWrapper>
                        <SearchIcon name="search" />
                        <Input value={this.state.inputNumber} onChange={e => this.setState({ inputNumber: e.target.value })} />
                        <span style={{ color: "lightgrey" }} > {this.state.inputType}</span>
                      </InputWrapper>
                      
                      {/* displays list of possible selections: {this.state.inputType === 'days' ? days.map(d => this.renderInputList(d, 'days')) : km.map(k => <ListItem>{k} km</ListItem>)} */}
                    </Form>
                  </List>)
                :
                this.state.inputType === "models"
                ?
                  (<List>
                    {models.map(v => this.renderInputList(v))}
                  </List>)
                :
                  this.state.inputType === 'date picker'
                ?
                    <List>
                      <Form onSubmit={this.saveFilter}>
                        <InputWrapper>
                          <SearchIcon name="search" />
                          <DatePicker type="date" value={this.state.inputSelected} onChange={e => this.setState({ inputSelected: e.target.value })} />
                          <SaveButton>Save</SaveButton>
                        </InputWrapper>
                      </Form>
                    </List>
                : null
              }

            </ListRenderer>

          </FilterWrapper>

        </FilterContainer>
      </ComponentWrapper>
    );
  }
}

export default Filter;
