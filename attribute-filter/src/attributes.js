module.exports = 
{
    "Rental left" : {
        "greater than" : "days",
        "less than" : "days",
        "is" : "days",
        "is unknown" : null
    },

    "Turn back milage" : {
        "greater than" : "km",
        "less than" : "km",
        "is" : "km",
        "is unknown" : null
    },

    "Agreement": {
        "more than" : "days",
        "exactly" :	"days",
        "less than"	: "days",
        "after"	: "date picker",
        "on" : "date picker",
        "before" : "date picker",
        "is unknown" : null,	
        "has any value": null
    },

    "Model": {
        "is" : "models",
        "is not": "models",
        "starts with":	"models",
        "ends with": "models",
        "contains":	"models",
        "does not contain":	"models",
        "is unknown": null,	
        "has any value": null	
    },

    "Vehicle maintenance needed" : {
        "is true" : null,	
        "is false": null,	
        "is unknown": null,	
        "has any value": null
    }

}


// [{
//         name: "Rental Left",
//         operator: []
//     },
//     {
//         name: "Turn Back Milage",
//         operator: "numberKm"
//     },
//     {
//         name: "Agreement",
//         operator: "date"
//     },
//     {
//         name: "Model",
//         operator: "string"
//     },
//     {
//         name: "Vehicle Maintenance Needed",
//         operator: "boolean"
//     }

// ]