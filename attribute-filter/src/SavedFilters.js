import React, { Component } from 'react';
import styled from 'styled-components'

const FilterWrap = styled.div`
    width: 25rem;

`

const FilterContainer = styled.div`
    ${'' /* background-color: rgb(217, 237, 254);
    width: 20rem;
    padding: 0.4rem 1rem; */}
    margin: 10px 0;
`
const FilterText = styled.span`
    background-color: rgba(186,216,255,.8);
    padding: 7px 0px 7px 10px;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;

`
const FilterTextBold = styled.span`
        background-color: rgba(186,216,255,.8);
        padding: 7px 1px;
        font-weight: 700;
`
const DeleteButton = styled.button`
     border: none;
     font-size: 1rem;
     background-color: rgba(186,216,255,.8);
     padding: 8.59px 10px 8.5px 40px;
     border-bottom-right-radius: 3px;
     border-top-right-radius: 3px;
`
class SavedFilters extends Component {

    renderFilter = (f) => {
        return (
            <FilterContainer>
                <FilterText>{f.attribute} {f.operator} </FilterText> 
                {f.input ? <FilterTextBold>{f.input}</FilterTextBold> : null}
                <DeleteButton title={f.filterId} onClick={this.props.deleteFilter}> X </DeleteButton>
            </FilterContainer>
        )
       
    }

   render() {
    return (
        <FilterWrap>
            {this.props.filterList.map(f => this.renderFilter(f)) }
        </FilterWrap>
    )
   } 
}

export default SavedFilters;