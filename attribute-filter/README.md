## Attribute Filter

Welcome to my filter component!

To get started, place clone the repo, then cd to attribute-filter, npm install to add the external libraries,  and hit npm start to see it in action.

#### Data Flow

The attributes, operators, input organized into a nested object in a separate file (attributes.js) that is imported into the component. This makes it easy to change the data and also makes accessing it faster than if it were in an array. The car models are also imported from a separate file (models.js).

1. In first column, the renderAttributesList checks to make sure the attribute file has been imported and then the object keys are mapped over and rendered as <li>s. Each one has a title property to make it targetable when the user clicks on it. Clicking the <li> sends its value to the state by the selectAttribute method, while also renders it as the new heading of the column and triggering hideAttribute(true), hideOperatorSection(false), and hideOperatorList(false).

2. As soon as there in an attribute in state, the renderOperatorList is triggered and functions similarly to renderAttributeList. When the user clicks on an operator, the operator and the operator's corresponding input type are added to the state by the selectOperator method which checks if the inputType is null, and if it is, it calls the saveFilter method.

3. Otherwise, it triggers the hideOperatorList(true) and hideInputSelection(false). The input section is then rendered, depending on the input type. When the user selects an input from the dropdown menu or puts one in, the same savefilter method is called.

4. saveFilter compiles each part of the filter into an object and concatenates it to the filter array in state. saveFilter also gives each object a random ID so that it can be targeted and deleted. Then it calls the clearPreviousFilterState method to revert some of the state properties back in preparation for the next filter.
    * The saved filters are rendered in their own component, with the deleteFilter method passed down through props so that it can manipulate the main component's state.

5. The deleteFilter method clones the filterList from state, then loops over it and removes the object where the filter id corresponds to the id of the targeted filter, and then it sets the filter list state to the clone. My understanding is that it's best practice to only change the state through setState.  

#### Libraries Used

* FontAwesome
* Styled Components

#### Up next

* I would add in a custom filter option so the user can put whatever they want into each section. I would add an object to the nested attributes object: "custom: {custom:custom}' and then render a black input field when the program reads the string "custom".

* I would transfer the third column (input) into its own component.

* I started writing the logic to have a dropdown menu selection options for input's days & km, so I would complete that. 



